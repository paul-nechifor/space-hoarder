# SpaceHoarder

A basic SpaceMonger clone for Linux written in Python with GTK+ 3.

![SpaceHoarder screenshot.](screenshot.png)

## License

MIT
